function bringData(){
    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", "dbStudents.json", true);
    xhttp.send();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        let answer = JSON.parse(this.responseText);
        student = answer.DatosEstudiantiles["estudiante"];
        
        let data = document.querySelector("#student-data");
        data.innerHTML = "";

        for (const item of student) {
            console.log(item.nombres);
            data.innerHTML += `<tr>
                                    <td>${item.cedula}</td>
                                    <td>${item.nombres}</td>
                                    <td>${item.apellidos}</td>
                                    <td>${item.direccion}</td>
                                    <td>${item.telefono}</td>
                                    <td>${item.correo}</td>
                                    <td>${item.curso}</td>
                                    <td>${item.paralelo}</td>
                                </tr>`;
        }
    }
};
}
bringData();


